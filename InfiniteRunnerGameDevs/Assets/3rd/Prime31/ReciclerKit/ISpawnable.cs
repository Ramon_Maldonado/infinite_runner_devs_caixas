using UnityEngine.EventSystems;

public interface ISpawnable : IEventSystemHandler
{
    void OnSpawn();
    void OnDespawn();
}

