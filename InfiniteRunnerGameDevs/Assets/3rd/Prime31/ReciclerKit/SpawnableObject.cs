﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnableObject
{
    public GameObject GameObjectInstance { get; set; }
    public List<ISpawnable> SpawnableInterfaces { get; set; }

    public SpawnableObject(GameObject gameObject)
    {
        SpawnableInterfaces = gameObject.GetComponents<ISpawnable>().ToList();
        if (SpawnableInterfaces == null)
            return;
        Debug.Log(SpawnableInterfaces.Count);
        AssignEvents();
    }

    public event Action OnSpawnEvent;
    public event Action OnDespawnEvent;

    private void AssignEvents()
    {
        foreach (var spawnable in SpawnableInterfaces)
        {
            OnSpawnEvent += spawnable.OnSpawn;
            OnDespawnEvent += spawnable.OnDespawn;
        }
    }

    private void UnassignEvents()
    {
        foreach (var spawnable in SpawnableInterfaces)
        {
            OnSpawnEvent -= spawnable.OnSpawn;
            OnDespawnEvent -= spawnable.OnDespawn;
        }
    }

    public void CallOnSpawn()
    {
        if (OnSpawnEvent != null)
            OnSpawnEvent();
    }

    public void CallOnDespawn()
    {
        if (OnDespawnEvent != null)
            OnDespawnEvent();
    }
}