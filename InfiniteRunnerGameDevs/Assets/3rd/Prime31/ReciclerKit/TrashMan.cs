using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Ez.Msg;
using MoreMountains.Tools;
using UnityEngine.EventSystems;


public class TrashMan : Singleton<TrashMan>
{

	/// <summary>
	/// stores the recycle bins and is used to populate the lookup Dictionaries at startup
	/// </summary>
	[HideInInspector]
	public List<TrashManRecycleBin> recycleBinCollection;

	/// <summary>
	/// this is how often in seconds TrashMan should cull excess objects. Setting this to 0 or a negative number will
	/// fully turn off automatic culling. You can then use the TrashManRecycleBin.cullExcessObjects method manually if
	/// you would still like to do any culling.
	/// </summary>
	public float cullExcessObjectsInterval = 0f;

	/// <summary>
	/// if true, DontDestroyOnLoad will be called on the TrashMan
	/// </summary>
	public bool persistBetweenScenes = false;

	/// <summary>
	/// uses the GameObject instanceId as its key for fast look-ups
	/// </summary>
	private Dictionary<int,TrashManRecycleBin> _instanceIdToRecycleBin = new Dictionary<int,TrashManRecycleBin>();

	/// <summary>
	/// uses the pool name to find the GameObject instanceId
	/// </summary>
	private Dictionary<string,int> _poolNameToInstanceId = new Dictionary<string,int>();

	[HideInInspector]
	public new Transform transform;


    public GameObject[] gameObjectsToAdd;


    protected static ExecuteEvents.EventFunction<ISpawnable> OnSpawn;
    protected static ExecuteEvents.EventFunction<ISpawnable> OnDespawn;

    #region MonoBehaviour

    protected override void Awake()
    {
		if ( _instance != this && _instance != null )
		{
			Destroy(gameObject);
		}
		else
		{
		    OnSpawn = (handler, data) => handler.OnSpawn();
		    OnDespawn = (handler, data) => handler.OnDespawn();
		    transform = gameObject.transform;

			initializePrefabPools();

			if ( persistBetweenScenes )
				DontDestroyOnLoad(gameObject);
		}

        // only cull if we have an interval greater than 0
		if ( cullExcessObjectsInterval > 0 )
			StartCoroutine(cullExcessObjects());
	}


	private void OnLevelWasLoaded()
	{
		for( var i = recycleBinCollection.Count - 1; i >= 0; i-- )
		{
			if( !recycleBinCollection[i].persistBetweenScenes )
				removeRecycleBin( recycleBinCollection[i] );
		}
	}


	private void OnApplicationQuit()
	{
		_instance = null;
	}

	#endregion


	#region Private

	/// <summary>
	/// coroutine that runs every couple seconds and removes any objects created over the recycle bins limit
	/// </summary>
	/// <returns>The excess objects.</returns>
	private IEnumerator cullExcessObjects()
	{
		var waiter = new WaitForSeconds( cullExcessObjectsInterval );

		while( true )
		{
			for( var i = 0; i < recycleBinCollection.Count; i++ )
				recycleBinCollection[i].cullExcessObjects();

			yield return waiter;
		}
	}


	/// <summary>
	/// populats the lookup dictionaries
	/// </summary>
	private void initializePrefabPools()
	{
		if( recycleBinCollection == null )
			return;

		foreach( var recycleBin in recycleBinCollection )
		{
			if( recycleBin == null || recycleBin.prefab == null )
				continue;

		    if (_instanceIdToRecycleBin.ContainsKey(recycleBin.prefab.GetInstanceID()))
		        continue;

		    recycleBin.initialize();
			_instanceIdToRecycleBin.Add( recycleBin.prefab.GetInstanceID(), recycleBin );
			_poolNameToInstanceId.Add( recycleBin.prefab.name, recycleBin.prefab.GetInstanceID() );
		}
	}

    public void addNewRecycleBin( GameObject go, int instancesToPreallocate )
    {
        if( recycleBinCollection == null )
            recycleBinCollection = new List<TrashManRecycleBin>();


        Debug.Log(string.Format("Adding Name: {0}", go.name), go);
        if (_instanceIdToRecycleBin.ContainsKey(go.GetInstanceID())
            || _poolNameToInstanceId.ContainsKey(go.name))
        {
            Debug.LogWarning(string.Format("GameObject already added to pool. Name: {0}", go.name), go);
            return;
        }

        var newPrefabPool = new TrashManRecycleBin();
        newPrefabPool.instancesToPreallocate = instancesToPreallocate;
        newPrefabPool.prefab = go;

        recycleBinCollection.Add( newPrefabPool );

        newPrefabPool.initialize();


        _instanceIdToRecycleBin.Add( newPrefabPool.prefab.GetInstanceID(), newPrefabPool );
        _poolNameToInstanceId.Add( newPrefabPool.prefab.name, newPrefabPool.prefab.GetInstanceID() );
    }


	/// <summary>
	/// internal method that actually does the work of grabbing the item from the bin and returning it
	/// </summary>
	/// <param name="gameObjectInstanceId">Game object instance identifier.</param>
	private static GameObject spawn( int gameObjectInstanceId, Vector3 position, Quaternion rotation )
	{
		if( Instance._instanceIdToRecycleBin.ContainsKey( gameObjectInstanceId ) )
		{
			var newGo = Instance._instanceIdToRecycleBin[gameObjectInstanceId].spawn();

			if( newGo != null )
			{
				var newTransform = newGo.transform;

#if UNITY_4_6 || UNITY_5_0
                if (newTransform as RectTransform)
                    newTransform.SetParent(null, false);
                else
#endif
				    newTransform.parent = null;

				newTransform.position = position;
				newTransform.rotation = rotation;

				newGo.SetActive( true );
			    newGo.SendSimple(OnSpawn);
			}

			return newGo;
		}

		return null;
	}


	/// <summary>
	/// internal coroutine for despawning after a delay
	/// </summary>
	/// <returns>The despawn after delay.</returns>
	/// <param name="go">Go.</param>
	/// <param name="delayInSeconds">Delay in seconds.</param>
	private IEnumerator internalDespawnAfterDelay( GameObject go, float delayInSeconds )
	{
		yield return new WaitForSeconds( delayInSeconds );
		despawn( go );
	}

	#endregion


	#region Public

	/// <summary>
	/// tells TrashMan to start managing the recycle bin at runtime
	/// </summary>
	/// <param name="recycleBin">Recycle bin.</param>
	public static void manageRecycleBin( TrashManRecycleBin recycleBin )
	{
		// make sure we can safely add the bin!
		if( Instance._poolNameToInstanceId.ContainsKey( recycleBin.prefab.name ) )
		{
			Debug.LogError( "Cannot manage the recycle bin because there is already a GameObject with the name (" + recycleBin.prefab.name + ") being managed" );
			return;
		}

		Instance.recycleBinCollection.Add( recycleBin );
		recycleBin.initialize();
		Instance._instanceIdToRecycleBin.Add( recycleBin.prefab.GetInstanceID(), recycleBin );
		Instance._poolNameToInstanceId.Add( recycleBin.prefab.name, recycleBin.prefab.GetInstanceID() );
	}


	/// <summary>
	/// stops managing the recycle bin optionally destroying all managed objects
	/// </summary>
	/// <param name="recycleBin">Recycle bin.</param>
	/// <param name="shouldDestroyAllManagedObjects">If set to <c>true</c> should destroy all managed objects.</param>
	public static void removeRecycleBin( TrashManRecycleBin recycleBin, bool shouldDestroyAllManagedObjects = true )
	{
		var recycleBinName = recycleBin.prefab.name;

		// make sure we are managing the bin first
		if( Instance._poolNameToInstanceId.ContainsKey( recycleBinName ) )
		{
			Instance._poolNameToInstanceId.Remove( recycleBinName );
			Instance._instanceIdToRecycleBin.Remove( recycleBin.prefab.GetInstanceID() );
			Instance.recycleBinCollection.Remove( recycleBin );
			recycleBin.clearBin( shouldDestroyAllManagedObjects );
		}
	}


	/// <summary>
	/// pulls an object out of the recycle bin
	/// </summary>
	/// <param name="go">Go.</param>
	public static GameObject spawn( GameObject go, Vector3 position = default( Vector3 ), Quaternion rotation = default( Quaternion ) )
	{
		if( Instance._instanceIdToRecycleBin.ContainsKey( go.GetInstanceID() ) )
		{
			return spawn( go.GetInstanceID(), position, rotation );
		}
		else
		{
			Debug.LogWarning( "attempted to spawn go (" + go.name + ") but there is no recycle bin setup for it. Falling back to Instantiate" );
			Instance.addNewRecycleBin(go, 0);
            var newGo = Instantiate( go, position, rotation );
            newGo.name = go.name;
		    newGo.SendSimple(OnSpawn);

#if UNITY_4_6 || UNITY_5_0
            if (newGo.transform as RectTransform != null)
                newGo.transform.SetParent(null, false);
            else
#endif
			    newGo.transform.parent = null;

			return newGo;
		}
	}


	/// <summary>
	/// pulls an object out of the recycle bin using the bin's name
	/// </summary>
	public static GameObject spawn( string gameObjectName, Vector3 position = default( Vector3 ), Quaternion rotation = default( Quaternion ) )
	{
		int instanceId = -1;
		if( Instance._poolNameToInstanceId.TryGetValue( gameObjectName, out instanceId ) )
		{
			return spawn( instanceId, position, rotation );
		}
		else
		{
			Debug.LogError( "attempted to spawn a GameObject from recycle bin (" + gameObjectName + ") but there is no recycle bin setup for it" );
			return null;
		}
	}


	/// <summary>
	/// sticks the GameObject back into its recycle bin. If the GameObject has no bin it is destroyed.
	/// </summary>
	/// <param name="go">Go.</param>
	public static void despawn( GameObject go )
	{
		if( go == null )
			return;

		var goName = go.name;
		if( !Instance._poolNameToInstanceId.ContainsKey( goName ) )
		{
			Destroy( go );
		}
		else
		{
		    go.SendSimple(OnDespawn);
		    Instance._instanceIdToRecycleBin[Instance._poolNameToInstanceId[goName]].despawn( go );

#if UNITY_4_6 || UNITY_5_0
            if (go.transform as RectTransform != null)
                go.transform.SetParent(instance.transform, false);
            else
#endif
                go.transform.parent = Instance.transform;
		}
	}


	/// <summary>
	/// sticks the GameObject back into it's recycle bin after a delay. If the GameObject has no bin it is destroyed.
	/// </summary>
	/// <param name="go">Go.</param>
	public static void despawnAfterDelay( GameObject go, float delayInSeconds )
	{
		if( go == null )
			return;

		Instance.StartCoroutine( Instance.internalDespawnAfterDelay( go, delayInSeconds ) );
	}


	/// <summary>
	/// gets the recycle bin for the given GameObject name. Returns null if none exists.
	/// </summary>
	public static TrashManRecycleBin recycleBinForGameObjectName( string gameObjectName )
	{
		if( Instance._poolNameToInstanceId.ContainsKey( gameObjectName ) )
		{
			var instanceId = Instance._poolNameToInstanceId[gameObjectName];
			return Instance._instanceIdToRecycleBin[instanceId];
		}
		return null;
	}


	/// <summary>
	/// gets the recycle bin for the given GameObject. Returns null if none exists.
	/// </summary>
	/// <returns>The bin for game object.</returns>
	/// <param name="go">Go.</param>
	public static TrashManRecycleBin recycleBinForGameObject( GameObject go )
	{
		if( Instance._instanceIdToRecycleBin.ContainsKey( go.GetInstanceID() ) )
			return Instance._instanceIdToRecycleBin[go.GetInstanceID()];
		return null;
	}


	#endregion

}
