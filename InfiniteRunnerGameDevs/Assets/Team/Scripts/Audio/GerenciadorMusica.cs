﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GerenciadorMusica : MonoBehaviour {

    private ReprodutorSFX _reprodutor;

    private static GerenciadorMusica Instancia = null;

    private void Awake()
    {
        if (Instancia == null)
            Instancia = this;

        else if (Instancia != null)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        //_reprodutor = GetComponent<ReprodutorSFX>();
    }

    //void Start()
    //{
    //    _reprodutor.Reproduzir("Musica");
    //}

    //public void ReproduzirMusica()
    //{
    //    _reprodutor.Reproduzir("Musica");
    //}
}
