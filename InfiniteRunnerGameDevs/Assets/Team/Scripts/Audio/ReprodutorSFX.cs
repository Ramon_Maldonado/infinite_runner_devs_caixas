﻿using System.Collections;
using Ez.Msg;
using UnityEngine;
using UnityEngine.EventSystems;

public interface IReprodutorSFX : IEventSystemHandler
{
    void Reproduzir(string sound);
    IEnumerable EzReproduzir(string sound);
}

public class ReprodutorSFX : MonoBehaviour, IReprodutorSFX
{

    public static ExecuteEvents.EventFunction<IReprodutorSFX> MsgReproduzir(string nome)
    {
        return (handler, data) => handler.Reproduzir(nome);
    }

    public static EzMsg.EventAction<IReprodutorSFX> EzMsgReproduzir(string nome)
    {
        return handler => handler.EzReproduzir(nome);
    }


    private AudioSource _audioSource;
    public string NomeSom;
    public GameObject Preset;
    public GameObject SomEspawnavel;

    public bool Aleatorio = false;
    public bool PitchAleatorio = false;
    public bool InterromperCanal = false;
    public bool CriarNovoObjeto = false;
    public float PitchMinimo = 0.95f;
    public float PitchMaximo = 1.05f;
    public AudioClip[] Clips;

    public float VolumeInicial { get; private set; }

    protected virtual void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        if (!_audioSource)
            _audioSource = gameObject.AddComponent<AudioSource>();

        VolumeInicial = _audioSource.volume;

        if (Preset)
        {
            AudioSource target = Preset.GetComponent<AudioSource>();
            _audioSource.CopyAudioSource(target);
        }
        _audioSource.clip = Clips.Length > 0 ? Clips[0] : null;
    }

    protected void RandomizarPitch()
    {
        _audioSource.pitch = Random.Range(PitchMinimo, PitchMaximo);
    }

    public virtual IEnumerable EzReproduzir(string nomeSom)
    {
        Reproduzir(nomeSom);
        yield break;
    }

    public void Reproduzir(string nomeSom)
    {
        int random = 0;
        if (!NomeSom.Equals(nomeSom)) return;
        if (PitchAleatorio) RandomizarPitch();
        if (Aleatorio) random = Random.Range(0, Clips.Length);

        _audioSource.clip = Clips[random];
        if (CriarNovoObjeto)
        {
            SpawnarSound();
            return;
        }
        if (!InterromperCanal)
            _audioSource.PlayOneShot(Clips[random]);
        else
            _audioSource.Play();
    }

    protected void SpawnarSound()
    {
        var go = TrashMan.spawn(SomEspawnavel, transform.position);
        go.Send(TrashCollectorAudio.EzMsgPLay(_audioSource));
    }

    public void BaixarVolume()
    {
        _audioSource.volume = 0.1f;
    }

    public void RestaurarVolume()
    {
        _audioSource.volume = VolumeInicial;
    }
}
