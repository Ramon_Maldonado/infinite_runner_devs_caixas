﻿using UnityEngine;

public class ReprodutorSFXOnVisible : ReprodutorSFX
{
    private AudioSource efxSource;
    public bool ReproduzirOnEnabled = false;
    public bool ReproduzirOnBecameVisible = true;
    public bool CancelarOnInvisible = false;

    void OnEnable()
    {
        if (ReproduzirOnEnabled) Reproduzir(NomeSom);
    }

    void OnDisable()
    {

    }

    void OnBecameVisible()
    {
        if (ReproduzirOnBecameVisible) Reproduzir(NomeSom);
    }

    void OnBecameInvisible()
    {
        if (CancelarOnInvisible) efxSource.Stop();
    }
}
