﻿using UnityEngine;
using System.Collections;
using Ez.Msg;
using UnityEngine.EventSystems;

public interface ITrashCollectorAudio : IEventSystemHandler
{
    void ReproduzirUmaVez(AudioSource source);
    void Reproduzir(AudioSource source);
    IEnumerable EzReproduzirUmaVez(AudioSource source);
    IEnumerable EzReproduzir(AudioSource source);
}

[RequireComponent(typeof(AudioSource))]
public class TrashCollectorAudio : MonoBehaviour, ITrashCollectorAudio
{
    public static ExecuteEvents.EventFunction<ITrashCollectorAudio> MsgPlay(AudioSource source)
    {
        return (handler, data) => handler.Reproduzir(source);
    }

    public static EzMsg.EventAction<ITrashCollectorAudio> EzMsgPLay(AudioSource source)
    {
        return handler => handler.EzReproduzir(source);
    }

    public static ExecuteEvents.EventFunction<ITrashCollectorAudio> MsgPlayOneShot(AudioSource source)
    {
        return (handler, data) => handler.ReproduzirUmaVez(source);
    }

    public static EzMsg.EventAction<ITrashCollectorAudio> EzMsgPLayOneShot(AudioSource source)
    {
        return handler => handler.EzReproduzirUmaVez(source);
    }

    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void ReproduzirUmaVez(AudioSource audioSource)
    {
        _audioSource.CopyAudioSource(audioSource);
        _audioSource.PlayOneShot(audioSource.clip);
        _audioSource.loop = false;
    }

    public void Reproduzir(AudioSource source)
    {
        _audioSource.CopyAudioSource(source);
        _audioSource.Play();
        _audioSource.loop = false;
    }

    public IEnumerable EzReproduzirUmaVez(AudioSource audioSource)
    {
        ReproduzirUmaVez(audioSource);
        yield break;
    }

    public IEnumerable EzReproduzir(AudioSource audioSource)
    {
        Reproduzir(audioSource);
        yield break;
    }

    private void DespawnAfter(float time)
    {
        TrashMan.despawnAfterDelay(gameObject, time);
    }

    private void LateUpdate()
    {
        if (!_audioSource.isPlaying)
            TrashMan.despawn(gameObject);
    }
}
