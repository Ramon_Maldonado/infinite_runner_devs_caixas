﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ControleCelula : MonoBehaviour , IRemoveCelula
{

    [SerializeField]
    private float velocidade = 1;
    public Transform pontoFinal;

    private void Awake()
    {
        
    }

    private void FixedUpdate()
    {
        transform.Translate(Vector2.left * Time.deltaTime * velocidade);
    }
    
    public void RemoverCelula()
    {
        Destroy(gameObject);
    }
}
