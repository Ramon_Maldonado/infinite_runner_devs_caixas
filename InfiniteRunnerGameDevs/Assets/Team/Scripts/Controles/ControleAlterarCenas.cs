﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.TransitionKit;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ControleAlterarCenas : MonoBehaviour
{

    public SceneField Cena;
    public SceneField CenaCreditos;
    public SceneField CenaPrincipal;

    private bool _emTransicao;
    public bool TelaInicial;
    public bool TelaCreditos;

    private void Start()
    {
        _emTransicao = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Transicao();                
        }
        else if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            TransicaoCreditos();
        }
    }

    public void Transicao()
    {
        if (_emTransicao)
            return;

        _emTransicao = true;

        var fader = new FadeTransition()
        {
            nextScene = Cena,
            fadeToColor = Color.black,
        };
        TransitionKit.instance.transitionWithDelegate(fader);
    }
    public void TransicaoCreditos()
    {
        if (_emTransicao)
            return;

        _emTransicao = true;

        var fader = new FadeTransition()
        {
            nextScene = CenaCreditos,
            fadeToColor = Color.black,
        };
        TransitionKit.instance.transitionWithDelegate(fader);
    }
}
