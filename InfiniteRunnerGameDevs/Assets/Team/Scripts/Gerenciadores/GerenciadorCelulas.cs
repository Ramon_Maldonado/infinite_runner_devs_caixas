﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GerenciadorCelulas : MonoBehaviour {

	public static GerenciadorCelulas Instancia = null;

    [SerializeField]
    public List<ControleCelula> Celulas;
    public ControleCelula _proximaCelula;

    //quando destru deve instanciar um e setar o ponto 


    private void Awake()
    {
        if (Instancia == null)
            Instancia = this;

        else if (Instancia != null)
            Destroy(gameObject);

        //DontDestroyOnLoad(gameObject);
        
    }


    private void Update()
    {
        
    }

    public ControleCelula GetCelulaAleatoria()
    {
        return Celulas[Random.Range(0, Celulas.Count)];
    }


    public void ProximaCelula()
    {
        var celula = GetCelulaAleatoria();
        var o = Instantiate(celula, _proximaCelula.pontoFinal.position,transform.rotation);
        _proximaCelula = o;
    }


}
