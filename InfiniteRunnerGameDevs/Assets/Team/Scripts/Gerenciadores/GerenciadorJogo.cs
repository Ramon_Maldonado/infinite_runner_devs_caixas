﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GerenciadorJogo : MonoBehaviour
{
    public static GerenciadorJogo Instancia = null;

    private int _pontos;

    private AudioSource _audioSource;

    [SerializeField]
    private Text Text;


    private void Awake()
    {
        if (Instancia == null)
            Instancia = this;

        else if (Instancia != null)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        _audioSource = GetComponent<AudioSource>();
    }

    public void ContarPonto(int ponto)
    {
        _pontos += ponto;
        _audioSource.PlayOneShot(_audioSource.clip);
        Text.text = _pontos.ToString();
    }

    public void ResetarPonto()
    {
        _pontos = 0;
    }
}
