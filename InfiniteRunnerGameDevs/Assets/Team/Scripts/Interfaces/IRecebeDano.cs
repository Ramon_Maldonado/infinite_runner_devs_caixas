﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRecebeDano
{
    void ReceberDano(int dano);
}
