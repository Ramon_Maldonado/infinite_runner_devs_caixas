﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public interface IColetaPontos : IEventSystemHandler
{
    IEnumerable ColetarPontos(int Pontos);
}

public class JogadorControle : MonoBehaviour , IRecebeDano, IColetaPontos 
{
    private Rigidbody2D _rb;

    [SerializeField]
    private float _gravidade = 1;
    [SerializeField]
    private float _energia = 1;

    [SerializeField]
    private float _posicaoAlvoX;
    [SerializeField]
    private float _velocidade = .01f;

    [SerializeField]
    private SpriteRenderer _spriteRenderer;

    [SerializeField]
    private Animator _animator;
    private bool _gravidadeInversa = false;

    [SerializeField]
    private float rayDistance = 1;

    [SerializeField]
    private LayerMask _groundLayer;

    [SerializeField]
    private CapsuleCollider2D _collider;

    [SerializeField]
    private GameObject _mortePrefab;

    private float _sinal;

    private int correndo = Animator.StringToHash("correndo");
    private int Pulando = Animator.StringToHash("Pulando");
    private int bola = Animator.StringToHash("bola");
    //private int Grounded = Animator.StringToHash("Grounded");


    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        IniciarGravidade();
    }

    void IniciarGravidade()
    {
        _rb.gravityScale = _gravidade; 
        _sinal = Mathf.Sign(_rb.gravityScale * -1);
    }

    private IEnumerator AlterarGravidade()
    {
        yield return null;
    }
    
    private void FixedUpdate()
    {

    }



    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Mouse0))
        {
            _gravidadeInversa = !_gravidadeInversa;
            _rb.gravityScale = _rb.gravityScale * -1;

            _sinal = Mathf.Sign(_rb.gravityScale * -1);
        }
        else if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Mouse0))
        {

        }


        Vector3 origin = transform.position;

        Vector3 direction;
        //if (Mathf.Abs(_rb.velocity.y) > 0.1)
        //    direction = (new Vector3(_rb.velocity.x, _rb.velocity.y, 0) * rayDistance * Time.deltaTime);
        //else
        direction = Vector3.up * rayDistance * _sinal;

        Vector2 end = origin + direction;

        Debug.DrawLine(origin, end, Color.red);

        if (Physics2D.Linecast(origin, end, _groundLayer))
        {
            _animator.Play(correndo);
            if (_sinal > 0)
                _spriteRenderer.flipY = true;
            else
                _spriteRenderer.flipY = false;
        }
        else
        {
            _animator.Play(bola);
        }

    }
    bool Morreu;

    public void ReceberDano(int dano)
    {
        if (Morreu)
            return;

        Morreu = true;

        _spriteRenderer.enabled = false;
        var go = Instantiate(_mortePrefab, transform.position, transform.rotation);
        go.transform.localScale = new Vector3(0.1f, -0.1f * _sinal, 0.1f);
        _collider.enabled = false;
        StartCoroutine(Morte());

        Debug.Log("recebendo dano");
    }

    private IEnumerator Morte()
    {
        yield return new WaitForSeconds(1.0f);
        GerenciadorJogo.Instancia.ResetarPonto();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public IEnumerable ColetarPontos(int pontos)
    {
        GerenciadorJogo.Instancia.ContarPonto(pontos);
        yield return null;
    }
}
