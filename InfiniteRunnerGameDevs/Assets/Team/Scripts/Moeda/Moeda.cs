﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ez.Msg;

public class Moeda : MonoBehaviour {

    private int _qtdPontos = 100;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        collision.gameObject.Send<IColetaPontos>(x => x.ColetarPontos(_qtdPontos));

        Destroy(gameObject);
    }
}
