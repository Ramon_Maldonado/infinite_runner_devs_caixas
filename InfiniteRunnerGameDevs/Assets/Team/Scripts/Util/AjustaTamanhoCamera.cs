﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AjustaTamanhoCamera : MonoBehaviour
{

    void Awake()
    {
        var camera = GetComponent<Camera>();
        camera.orthographicSize = 5 * 1 / (camera.aspect / (16.0f / 9.0f));
    }

}
