﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public static class Extensions  {
    public static void Clamp(this float number, float min , float max)
    {
        number = Mathf.Clamp(number, min, max);
    }

    public static float Clamped(this float number, float min , float max)
    {
        return Mathf.Clamp(number, min, max);
    }

    public static float Clamped(this int number, float min , float max)
    {
        return Mathf.Clamp(number, min, max);
    }

    public static T CopyComponentPublicValues<T>(this MonoBehaviour mono, T target, T source) where T : Component {
        FieldInfo[] sourceFields = source.GetType().GetFields(BindingFlags.Public);

        int i;
        for(i = 0; i < sourceFields.Length; i++) {
            var value = sourceFields[i].GetValue(source);
            sourceFields[i].SetValue(target, value);
        }

        return target;
    }

    public static void CopyAudioSource(this AudioSource target, AudioSource source) {
        target.clip = source.clip;
        target.outputAudioMixerGroup = source.outputAudioMixerGroup;
        target.mute = source.mute;
        target.bypassEffects = source.bypassEffects;
        target.bypassListenerEffects = source.bypassListenerEffects;
        target.bypassReverbZones = source.bypassReverbZones;
        target.playOnAwake = source.playOnAwake;
        target.loop = source.loop;
        target.priority = source.priority;
        target.volume = source.volume;
        target.pitch = source.pitch;
        target.panStereo = source.panStereo;
        target.spatialBlend = source.spatialBlend;
        target.reverbZoneMix = source.reverbZoneMix;
        target.dopplerLevel = source.dopplerLevel;
        target.rolloffMode = source.rolloffMode;
        target.minDistance = source.minDistance;
        target.spread = source.spread;
        target.maxDistance = source.maxDistance;
    }

    public static T RandomItem<T>(this T[] target) {
        int randomItem = UnityEngine.Random.Range(0, target.Length);
        return target[randomItem];
    }

    public static T RandomItem<T>(this List<T> target) {
        int randomItem = UnityEngine.Random.Range(0, target.Count);
        return target[randomItem];
    }

    public static T PopRandomItem<T>(this List<T> target) {
        T randomItem = target.RandomItem<T>();
        target.Remove(randomItem);
        return randomItem;
    }

    public static T PopFirstItem<T>(this List<T> target) where T : class
    {
        if (target.Count == 0)
            return null;
        T firstItem = target.First();
        target.Remove(firstItem);
        return firstItem;
    }

    public static void BoolToTrigger(this Animator animator, int hash, bool value)
    {
        if(value)
            animator.SetTrigger(hash);
        else
            animator.ResetTrigger(hash);
    }
}
