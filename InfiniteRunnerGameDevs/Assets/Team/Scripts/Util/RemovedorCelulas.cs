﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemovedorCelulas : MonoBehaviour
{
    public static event Action onRemoverCelua;
    
    private void OnTriggerExit2D(Collider2D collision)
    {
        var celula = collision.GetComponentInParent<IRemoveCelula>();
        if (celula != null)
        {
            GerenciadorCelulas.Instancia.ProximaCelula();
            celula.RemoverCelula();
        }   
    }
}
